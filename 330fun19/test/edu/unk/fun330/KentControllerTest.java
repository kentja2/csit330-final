package edu.unk.fun330;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import edu.unk.fun330.Ship;
import edu.unk.fun330.Universe;
import edu.unk.fun330.base.*;
import edu.unk.fun330.controllers.KentController;

/**
 * This junit test class tests the KentController class.
 * 
 * @author julie kent
 *
 */
class KentControllerTest {

	Universe u;
	Ship ship;
	ShipController controller;
	ControllerAction action;

	@BeforeAll
	static void beforeAll() { } // Run once before all test methods

	/**
	 * Run before each of the test methods to configure the environment.
	 * NOTE: the order of execution of the test cases is not predetermined
	 */
	@BeforeEach
	void beforeEach() {
		System.out.println("Before each test method");
		// Configure the base environment for the test cases
		// Create universe, add ship to it, and link controller to ship
		u = new Universe();
		ship = new Ship(1200,800,0);  // ship tied to this controller under test
		u.add(ship);
		controller = new KentController(ship);
		ship.setShipController(controller);
	}

	@AfterEach
	void afterEach() { } // Run after each of the test methods

	@AfterAll
	static void afterAll() { } // Run once after all test methods have completed

	/** Testing the flight adjustment to make sure it is moving toward a specific position
	 */
	@Test
	void testFlightAdjustment() {
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); // first action
		FlightAdjustment fa = (FlightAdjustment)action; //expected flight adjustment
		assertEquals(fa.getFacing(),(float)Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), 200, 200) + Util.PI / 2);
		assertEquals(fa.getAcceleration(),.05f);
	}

	/**
	 * Testing to make sure it fire a bullet when it can and is supposed to
	 */
	@Test
	void testFireBullet() {
		Ship ship2 = new Ship(1150,750,1);
		u.add(ship2);
		if (ship.canFire() && !ship.canFireLaser()) {
			action = controller.makeMove(u);
			assertTrue(action instanceof FireBullet);
		}
	}
	
	/**
	 * Testing to make sure it fire a laser when it can and is supposed to
	 */
	@Test
	void testFireLaser() {
		Ship ship2 = new Ship(1150,750,1);  // second ship for testing
		u.add(ship2);
		if (ship.canFireLaser()) {
			action = controller.makeMove(u);
			assertTrue(action instanceof FireLaser);
		}
	}
	
	/**
	 * Testing to make sure it fire Emp when it can and is supposed to
	 */
	@Test
	void testFireEMP() {
		Ship ship2 = new Ship(1150,750,1);
		u.add(ship2);
		if (ship.canFireEMP() && !ship.canFire() && !ship.canFireEMP()) {
			action = controller.makeMove(u);
			assertTrue(action instanceof FireEMP);
		}
	}
	
	/**
	 * Testing to make sure it toggles the shield when it can
	 */
	@Test
	void testShield() {
		if (ship.canShield()) {
			action = controller.makeMove(u);
			assertTrue(action instanceof ToggleShield);
		}
	}
	
	/**
	 * Testing to make sure it flies away from the black hole
	 */
	@Test
	void testBlackHole() {
		FlyingObject blackHole = new BlackHole(1150,750,u);
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment);
		FlightAdjustment fa = (FlightAdjustment)action;
		assertEquals(fa.getFacing(),(float)blackHole.getHeading() - Util.PI / 2f);
		assertEquals(fa.getAcceleration(),.05f);
	}
	
	/**
	 * Testing to make sure it flies away from the bomb
	 */
	@Test
	void testBombBonus() {
		FlyingObject bombBonus = new BombBonus(1150,750,1);
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); 
		FlightAdjustment fa = (FlightAdjustment)action; 
		assertEquals(fa.getFacing(),(float)bombBonus.getHeading() - Util.PI / 2f);
		assertEquals(fa.getAcceleration(),.05f);
	}
	
	/**
	 * Testing to make sure it flies towards a bullet bonus
	 */
	@Test
	void testBulletBonus() {
		FlyingObject object = new BulletBonus(1150, 750, 1);
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); 
		FlightAdjustment fa = (FlightAdjustment)action; 
		assertEquals(fa.getFacing(),(float)Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), object.getX(), object.getY())+ Util.PI / 2);
		assertEquals(fa.getAcceleration(),.05f);
	}
	
	/**
	 * Testing to make sure it flies towards an emp bonus
	 */
	@Test
	void testEmpBonus() {
		FlyingObject object = new EmpBonus(1150, 750, 1);
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); 
		FlightAdjustment fa = (FlightAdjustment)action; 
		assertEquals(fa.getFacing(),(float)Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), object.getX(), object.getY())+ Util.PI / 2);
		assertEquals(fa.getAcceleration(),.05f);
	}
	
	/**
	 * Testing to make sure it flies towards a fuel bonus
	 */
	@Test
	void testFuelBonus() {
		FlyingObject object = new FuelBonus(1150, 750, 1);
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); 
		FlightAdjustment fa = (FlightAdjustment)action; 
		assertEquals(fa.getFacing(),(float)Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), object.getX(), object.getY())+ Util.PI / 2);
		assertEquals(fa.getAcceleration(),.05f);
	}
	
	/**
	 * Testing to make sure it flies towards a laser bonus
	 */
	@Test
	void testLaserBonus() {
		FlyingObject object = new LaserBonus(1150, 750, 1);
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); 
		FlightAdjustment fa = (FlightAdjustment)action; 
		assertEquals(fa.getFacing(),(float)Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), object.getX(), object.getY())+ Util.PI / 2);
		assertEquals(fa.getAcceleration(),.05f);
	}
	
	/**
	 * Testing to make sure it flies towards a magnet bonus
	 */
	@Test
	void testMagnetBonus() {
		FlyingObject object = new MagnetBonus(1150, 750, 1);
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); 
		FlightAdjustment fa = (FlightAdjustment)action; 
		assertEquals(fa.getFacing(),(float)Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), object.getX(), object.getY())+ Util.PI / 2);
		assertEquals(fa.getAcceleration(),.05f);
	}
	
	/**
	 * Testing to make sure it flies towards a points bonus
	 */
	@Test
	void testPointsBonus() {
		FlyingObject object = new PointsBonus(1150, 750, 1);
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); 
		FlightAdjustment fa = (FlightAdjustment)action; 
		assertEquals(fa.getFacing(),(float)Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), object.getX(), object.getY())+ Util.PI / 2);
		assertEquals(fa.getAcceleration(),.05f);
	}
	
	/**
	 * Testing to make sure it flies towards a shield bonus
	 */
	@Test
	void testShieldBonus() {
		FlyingObject object = new ShieldBonus(1150, 750, 1);
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); 
		FlightAdjustment fa = (FlightAdjustment)action; 
		assertEquals(fa.getFacing(),(float)Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), object.getX(), object.getY())+ Util.PI / 2);
		assertEquals(fa.getAcceleration(),.05f);
	}
	
	/**
	 * Testing to make sure it flies away from the edge of the universe
	 */
	@Test
	void testEdgeOfUniverse() {
		ship.setX(100);
		action = controller.makeMove(u);
		assertTrue(action instanceof FlightAdjustment); 
		FlightAdjustment fa = (FlightAdjustment)action; 
		assertEquals(fa.getFacing(),(float)Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), Universe.WIDTH / 2, Universe.HEIGHT / 2)+ Util.PI / 2);
		assertEquals(fa.getAcceleration(),.05f);
	}
}
