package edu.unk.fun330.controllers;

import java.util.ArrayList;

import edu.unk.fun330.*;
import edu.unk.fun330.base.ControllerAction;
import edu.unk.fun330.base.FireBullet;
import edu.unk.fun330.base.FlightAdjustment;
import edu.unk.fun330.base.ShipController;
import edu.unk.fun330.base.*;


public class KentController extends ShipController{
	
	boolean fire = false;
	ArrayList<ControllerAction> steps = new ArrayList<>();

	public KentController(Ship ship) {
		super(ship);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return "Julie";
	}

	@Override
	public ControllerAction makeMove(Universe gd) {
		// TODO Auto-generated method stub
		
		if(steps.size() > 0) {
			ControllerAction action = steps.remove(0);
			return action;
		} else {
			
		FlightAdjustment fa = new FlightAdjustment();

		FlyingObject closestShip = ship;
		FlyingObject closestBonus = ship;
		float closestShipDistance = Float.MAX_VALUE;
		float closestBonusDistance = Float.MAX_VALUE;
		float facing = 0;
		
		for (FlyingObject flyingObject : gd.getFlyingObjects()) {
			
			if(ship.equals(flyingObject)) continue;
			
			float distance = (float)Math.sqrt(
					Math.pow(ship.getX() - flyingObject.getX(),2.0) + 
					Math.pow(ship.getY() - flyingObject.getY(),2.0));
			
			if(flyingObject instanceof Ship && distance < closestShipDistance) {
				closestShip = flyingObject;
				closestShipDistance = distance;
			}
			
			else if(flyingObject instanceof Bonus && !(flyingObject instanceof BombBonus) && distance < closestBonusDistance) {
				closestBonus = flyingObject;
				closestBonusDistance = distance;
			}
			
			else if((flyingObject instanceof BlackHole || flyingObject instanceof BombBonus) && distance < 300 ) {
				facing = flyingObject.getHeading() - Util.PI / 2f;
				fa.setFacing(facing);
				fa.setAcceleration(-.05f);
				return fa;
			}
		}
			if(ship.canShield()) {
				steps.add(new ToggleShield());
			}
			if(ship.getX() < 200 || ship.getX() > Universe.WIDTH - 200 || ship.getY() < 200 || ship.getY() > Universe.HEIGHT - 200) {
				facing = Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), Universe.WIDTH / 2, Universe.HEIGHT / 2)+ Util.PI / 2;
			}
			if(closestShipDistance < 400) { //targets ship
				facing = Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), closestShip.getNextX((int) (closestShipDistance / 10)),closestShip.getNextY((int) (closestShipDistance / 10)))+ Util.PI / 2;
			if(ship.canFireLaser() && closestShipDistance < 450 && closestShip != ship) {
				steps.add(new FireLaser());
			} else if(ship.canFire() && closestShipDistance < 450 && closestShip != ship) {
				steps.add(new FireBullet());
			} else if(ship.canFireEMP() && closestShipDistance < 450 && closestShip != ship) {
				steps.add(new FireEMP());}} 
			else { //no ship close then goes towards closest bonus
				facing = Util.PI * 2f - Util.findAngle(ship.getX(),ship.getY(), closestBonus.getX(), closestBonus.getY())+ Util.PI / 2;
			}
			
			
	
		
		fa.setFacing(facing);
		
		fa.setAcceleration(.05f);
		
		return fa;
		}
	}

}
