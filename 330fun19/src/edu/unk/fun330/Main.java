package edu.unk.fun330;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import javax.swing.*;

public class Main extends JFrame {

	Visualizer v;
	Engine e = null;
	Vector<String> controllers;

	public void init () {

		controllers = getControllers();

		v = new Visualizer();

		Container c = getContentPane();	
		c.add(v);

		try { 
			e = new Engine(v, controllers); 
			this.addKeyListener(e);
		}
		catch (Exception e) { System.err.println("Engine creation failed"); }
		addKeyListener(e);
	}

	public void start() { e.run(); }

	public void stop() { e.stop(); }

	public static void main(String[] args) throws Exception {

		//System.out.println("Width="+Constants.width);
		//System.out.println("Height="+Constants.height);

		final Main f = new Main();
		//final JFrame f = new JFrame();

		f.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		f.addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				if (JOptionPane.showConfirmDialog(f, 
						"Are you sure to close this window?", "Really Closing?", 
						JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
					/*
					System.out.println("flying objects:");
					for (FlyingObject flyingObject : f.e.gd.getFlyingObjects()) {
						System.out.println("object = "+flyingObject);
					}
					System.out.println("ships:");
					for (FlyingObject ship : f.e.gd.getShips()) {
						System.out.println("ship = "+ship);
					}
					*/
					System.exit(0);
				}
			}
		});

		//Container c = f.getContentPane();
		//Main main = new Main();

		//Animation a = new Animation();
		//Constants.asApplet = false;
		f.init();
		//c.add(main);
		//c.add(v);
		f.setSize(Constants.width, Constants.height);
		f.setTitle("330 Fun");
		//f.show();
		f.setVisible(true);

		/*
    	while (f == null) {
    		try {Thread.sleep(10); }
    		catch (Exception ex) {}
    	}
		 */
		f.start();

		//JFrame frame = new JFrame("I can\'t think of anything good");

		/*
      javax.swing.SwingUtilities.invokeLater(new Runnable() {
          public void run() {
              createAndShowGUI();
          }
      });
		 */

	}

	private Vector<String> getControllers() {	
		ControllerSelecterFrame csf = new ControllerSelecterFrame( );
		csf.setVisible(true);
		return csf.getSelections();
	}

	private class ControllerSelecterFrame extends JDialog {

		Button okButton;
		GridBagLayout layout;
		JLabel heading; //, spacing;
		JList<String> jlist;

		// return a Vector containing the names of the selected controllers (as Strings)
		// Modified 11-18-2019 due to deprecation of getSelectedValues
		public Vector<String> getSelections() {
			//Object [] objselections = jlist.getSelectedValues(); 
			List<String> objselections = jlist.getSelectedValuesList();
			/*
			Vector<String> selections = new Vector<String>() ;
			for (int i=0; i<objselections.length; i++)
				if (objselections[i] instanceof String)
					selections.add( (String)objselections[i]);
			return selections;
			*/
			return new Vector<String>(objselections);
		}


		public ControllerSelecterFrame() {
			super( (Frame)null, "Welcome", true);
			setSize(400,400);
			
			//Vector<String> classNames = new Vector<String>();
			//Vector<Class> controllerClasses = new Vector<Class>();
			Vector<String> controllers = new Vector<String>();

			try {
				// Find controllers in jar file
				Collection<Class> jarcontrollers = getClassesForPackage("edu.unk.fun330.controllers");
				//controllerClasses.addAll(jarcontrollers);
				for (Class c : jarcontrollers) {
					controllers.add(c.getName());
					//System.out.println(c);   

				}

				// Find controllers in src controller folder
				Collection<Class> srcClasses = getClasses("edu.unk.fun330.controllers");
				//controllerClasses.addAll(srcClasses);
				for (Class c : srcClasses)
					controllers.add(c.getName());
				//System.out.println(c);

			}
			catch (Exception e) {e.printStackTrace();}

			//Vector<String> vector = new Vector<String>();
			//Vecotr[] classNames = controllerClasses.toArray(new String[controllerClasses.size()]);

			/*
	    	// get list of controllers, only those which compile properly
	    	File controllerPath = new File("controllers");
	    	String [] files = controllerPath.list();

	    	for (int i = 0; i< files.length; i++) {
	    		String filename = files[i];
	    		int strLength = filename.length();
	    		if (strLength > 6) {
	    			String ext = filename.substring(strLength - 6, strLength);
	    			if ( ext.equals(".class") && filename.indexOf('$')< 0 ) {
	    				String className = filename.substring(0, strLength - 6);
	    				System.out.println("client: potential controller classname " + className);
	    				classNames.add(className);
	    			}
	    		}
	    	}
			 */

			this.setLayout(new BorderLayout(10, 10));
			layout = new GridBagLayout();
			//this.setLayout(layout);

			JPanel north = new JPanel();
			north.setLayout(layout);
			JPanel center = new JPanel();
			center.setLayout(layout);
			JPanel south = new JPanel();
			south.setLayout(layout);

			//jlist = new JList(classNames);
			//jlist = new JList(controllerClasses);
			jlist = new JList<String>(controllers);

			final GridBagConstraints constraints = new GridBagConstraints();

			constraints.gridx = 0; // note: gridy == RELATIVE 
			constraints.fill = 0;

			heading = new JLabel("Please select the controller(s) that you wish to use:");
			//spacing = new JLabel(" h");

			okButton = new Button("OK");
			okButton.addActionListener(new
					ActionListener() 
			{
				//boolean joinedGame = false;
				public void actionPerformed(ActionEvent event) {
					ControllerSelecterFrame.this.dispose();  // close the dialog when the user clicks OK
				}
			} );

			Container contentPane = this.getContentPane();

			//contentPane.add(heading);
			north.add(heading);
			layout.setConstraints(heading, constraints);

			//contentPane.add(spacing);
			//.add(spacing);
			//layout.setConstraints(spacing, constraints);

			//contentPane.add(jlist);
			center.add(jlist);
			layout.setConstraints(jlist, constraints);

			//contentPane.add(spacing);
			//layout.setConstraints(spacing, constraints);

			//contentPane.add(okButton);
			south.add(okButton);
			layout.setConstraints(okButton, constraints);

			contentPane.add(north,BorderLayout.NORTH);
			contentPane.add(center,BorderLayout.CENTER);
			contentPane.add(south,BorderLayout.SOUTH);
			this.pack();

			// include to make window close when user hits X box
			addWindowListener(new WindowAdapter()
			{ public void windowClosing(WindowEvent e)
			{ System.exit(0); }
			} );

		}

		/**
		 * Scans the current class path for the 330Controllers jar file, and then scans
		 * the for all the classes directly under the package name in question.
		 * Package name should be "edu.unk.fun330.controllers". Maybe should use URI instead of
		 * URL
		 * 
		 * Old approach which no longer works with Java 9 due to changes in Java:
		 * Scans all classloaders for the current thread for loaded jars, and then scans
		 * each jar for the package name in question, listing all classes directly under
		 * the package name in question. Assumes directory structure in jar file and class
		 * package naming follow java conventions (i.e. com.example.test.MyTest would be in
		 * /com/example/test/MyTest.class)
		 */
		public Collection<Class> getClassesForPackage(String packageName) throws Exception {
			Set<URL> jarUrls = new HashSet<URL>();
			String[] pathElements = System.getProperty("java.class.path").split(System.getProperty("path.separator"));
			for (String path: pathElements) {
				//System.out.println(path);
				//if (path.endsWith("330Controllers.jar")) {
				if (path.contains("330Controllers")) {
					File f = new File(path);
					//URL u = f.toURL();   //modified 11-19-2019 due to warning
					URL u = f.toURI().toURL();
					//System.out.println(u);
					jarUrls.add(u);
				}
			}
			String packagePath = packageName.replace(".", "/");
			
			/* REMOVED DUE TO CHANGES IN JAVA 9
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			Set<URL> jarUrls = new HashSet<URL>();

			while (classLoader != null) {
				if (classLoader instanceof URLClassLoader)
					for (URL url : ((URLClassLoader) classLoader).getURLs()) {
						System.out.println(url);
						if (url.getFile().endsWith(".jar")) {  // may want better way to detect jar files
							System.out.println(url);
							jarUrls.add(url);
						}
					}

				classLoader = classLoader.getParent();
			}
			*/

			Set<Class> classes = new HashSet<Class>();

			for (URL url : jarUrls) {
				JarInputStream stream = new JarInputStream(url.openStream()); // may want better way to open url connections
				JarEntry entry = stream.getNextJarEntry();

				while (entry != null) {
					String name = entry.getName();
					int i = name.lastIndexOf("/");

					if (i > 0 && name.endsWith(".class") && name.substring(0, i).equals(packagePath)) 
						classes.add(Class.forName(name.substring(0, name.length() - 6).replace("/", ".")));

					entry = stream.getNextJarEntry();
				}
				stream.close();
			}
			return classes;
		}

		/**
		 * Scans all classes accessible from the context class loader which belong to the given package and subpackages.
		 *
		 * @param packageName The base package
		 * @return The classes
		 * @throws ClassNotFoundException
		 * @throws IOException
		 */
		private Vector<Class> getClasses(String packageName)
				throws ClassNotFoundException, IOException {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			assert classLoader != null;
			String path = packageName.replace('.', '/');
			Enumeration<URL> resources = classLoader.getResources(path);
			List<File> dirs = new ArrayList<File>();
			while (resources.hasMoreElements()) {
				URL resource = resources.nextElement();
				try {
					File f = Paths.get(resource.toURI()).toFile();
					dirs.add(f);
				}
				catch (Exception e) {}
			}
			Vector<Class> classes = new Vector<Class>();
			for (File directory : dirs) {
				classes.addAll(findClasses(directory, packageName));
			}
			return classes;
		}

		/**
		 * Recursive method used to find all classes in a given directory and subdirs.
		 *
		 * @param directory   The base directory
		 * @param packageName The package name for classes found inside the base directory
		 * @return The classes
		 * @throws ClassNotFoundException
		 */
		private List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
			List<Class> classes = new ArrayList<Class>();

			if (!directory.exists()) {
				return classes;
			}
			File[] files = directory.listFiles();
			for (File file : files) {
				if (file.isDirectory()) {
					assert !file.getName().contains(".");
					classes.addAll(findClasses(file, packageName + "." + file.getName()));
				} else if (file.getName().endsWith(".class")) {
					classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
				}
			}
			return classes;
		}
	}
}