package edu.unk.fun330;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import edu.unk.fun330.base.*;
import edu.unk.fun330.base.effects.*;

public class Ship extends FlyingObject {

	protected int score;
	private boolean alive;
	protected ShipController shipController;
	private int fireBulletWait;
	private static int BULLET_FIRE_WAIT = 5;
	protected int bulletFireWait;
	
	protected int bullets;
	public static final int INIT_BULLETS = 50;
	private int highestBullets = INIT_BULLETS;

	protected int laserEnergy;
	protected static final int INIT_LASER = 5;
	private static final int HIGHEST_LASER = 10;
	
	protected int fuel;
	public static final int INIT_FUEL = 2000;
	private int highestFuel = INIT_FUEL;
	protected int lives;
	public static final int INIT_LIVES = 3;
	private int highestLives = INIT_LIVES;
	
	private int empCountdown = 0;
	private int jumpCountdown = 0;
	private int magnetCountdown = 0;
	
	/** @return The number of iterations before a held EMP bonus expires. 0 indicates expired. */
	public int getEmpCountdown() { return empCountdown; }
	
	/** @return The number of iterations before a held ShipJump bonus expires. 0 indicates expired. */
	public int getJumpCountdown() { return jumpCountdown; }
	
	/** @return The number of iterations before a held Magnet bonus expires. 0 indicates expired. */
	public int getMagnetCountdown() { return magnetCountdown; }

	private int disabledCountdown = 0;
	
	protected static final int INIT_DISABLED = 150;
	protected static final int INIT_MAGNETIC = 150;
	protected static final int INIT_EMP = 150;
	protected static final int INIT_JUMP = 150;
	
	//private int shieldWait;
	//private static final int SHIELD_WAIT = 30;
	//private int shieldCurrent;
	//private static final int SHIELD_DURATION = 600;
	//private int shieldLeft;
	//private static final int SHIELD_FOR_LIFE = 3000;

	protected boolean shieldUp;
	protected int shield;
	public static final int SHIELD_HEALTH_FOR_LIFE = 1000;
	private int highestShield = SHIELD_HEALTH_FOR_LIFE;
	
	protected int sleepTime;
	private static final int SLEEP_TIME = 150;
	
	protected static Image shipImage;
	protected int type;
	protected int weaponLevel;

	/** @return The shipImage. */
	public Image getShipImage() { return shipImage; }
	
	public boolean canShield() { return shield > 0; }
	public boolean isShieldUp() { return shieldUp; }

	protected void toggleShield() { 
		if (!isShieldUp() && canShield())
			shieldActivated();
		else shieldDeactivated();
	}
	
	private void shieldActivated() { shieldUp = true;	}
	private void shieldDeactivated() { shieldUp = false;	}

	public boolean canFire() { return fireBulletWait == 0 && bullets > 0; }
	public boolean canFireLaser() { return fireBulletWait == 0 && laserEnergy > 0; }
	public boolean canFireEMP() { return empCountdown > 0; }
	
	protected void bulletFired() { 
		fireBulletWait = bulletFireWait(); 
		if (!(this instanceof AIShip))
			bullets -= 1;
	}
	
	protected void laserFired() { 
		fireBulletWait = bulletFireWait(); 
		if (!(this instanceof AIShip))
			laserEnergy -= 1;
	}
	
	protected void empFired() {  empCountdown = 0;  }

	protected int bulletFireWait() { return BULLET_FIRE_WAIT; }
	
	public Ship(float x, float y, int type) {
		super(x, y);
		this.type = type;
		alive = true;
		fireBulletWait = 0;
		score = 0;
		resetLife();
	}

	protected void resetLife() {
		shield = SHIELD_HEALTH_FOR_LIFE;
		shieldUp = false;
		sleepTime = 0;
		weaponLevel = 1;
		laserEnergy = INIT_LASER;
		bullets = INIT_BULLETS;
		fuel = INIT_FUEL;
		empCountdown = 0;
		magnetCountdown = 0;
		disabledCountdown = 0;
		jumpCountdown = 0;
		if (lives == 0) lives = INIT_LIVES;  // reset lives after complete destruction & respawn
	}

	public int getScore() {return score; }
	public boolean isSleeping() { return sleepTime > 0; }
	public boolean isDisabled() { return disabledCountdown > 0; }
	public boolean isMagnetic() { return magnetCountdown > 0; }

	protected boolean intersects(FlyingObject fo) {
		if (isAlive())
			return super.intersects(fo);
		else return false;
	}
	
	private boolean hitRecently = false;
	
	// have the object react to being hit by another flying object
	protected boolean hitBy (FlyingObject fo, Universe u) {
		if (fo == this) return false;
		
		hitRecently = true;
		//if (fo instanceof Ship) {
		/*
		if (fo instanceof BlackHole) {
			lives--;
			if (lives == 0) die();
			else semiDie();
			return true;
		}
		*/
		//System.out.println("fo = "+fo);
		if (fo instanceof LaserBonus) {
			laserEnergy += ((LaserBonus)fo).getAmount();
			if(laserEnergy > HIGHEST_LASER)
				laserEnergy = HIGHEST_LASER;
			return false;
		}
		else if (fo instanceof BulletBonus) {
			bullets += ((BulletBonus)fo).getAmount();
			weaponLevel++;
			if(bullets > INIT_BULLETS)
				highestBullets = bullets;
			return false;
		}
		else if (fo instanceof FuelBonus) {
			fuel += ((FuelBonus)fo).getAmount();
			if(fuel > INIT_FUEL)
				highestFuel = fuel;
			return false;
		}
		/*
		else if (fo instanceof LifeBonus && !(this instanceof AIShip) ) {			
			lives += ((LifeBonus)fo).getAmount();
			score += Constants.RESPAWN_PENALTY;
			if(lives > INIT_LIVES)
				highestLives = lives;
			return false;
		}
		*/
		else if (fo instanceof PointsBonus) {
			score += ((PointsBonus)fo).getAmount();
			return false;
		}
		else if (fo instanceof EmpBonus) {
			empCountdown = INIT_EMP;
			return false;
		}
		else if (fo instanceof ShipJumpBonus) {
			jumpCountdown = INIT_JUMP;
			return false;
		}
		else if (fo instanceof MagnetBonus) {
			magnetCountdown = INIT_MAGNETIC;
			return false;
		}
		else if (fo instanceof ShieldBonus) {
			shield += ((ShieldBonus)fo).getAmount();
			if(shield > SHIELD_HEALTH_FOR_LIFE)
				highestShield = shield;
			return false;
		}
		else if (fo instanceof EMP) {
			EMP e = (EMP)fo;
			if (e.getShipCreator() == this) return false;
			//System.out.println("this="+this.getShipController().getName());
			this.disabledCountdown = INIT_DISABLED;
			if (this.isShieldUp()) this.toggleShield(); 
			return false;
		}
		else if (this.isShieldUp()) {
			
			if (shield > 100)
				shield -=100;
			else  {
				shield = 0;
				this.toggleShield();  // turn shield off
			}
			return false;
		}
		else {
			lives--;
			if(Constants.log) Constants.logWriter.logShipDeath(this,fo);
			//u.getExplosion().add(new Explosion(this.getX(),this.getY()));
			u.addExplosion(this.getX(),this.getY());  // ****should be throwing an event with the universe or engine being a listener
			if (Constants.SOUND) Sound.soundShipExplode.play();
			
			if (fo instanceof Bullet) {
				int deduction = (int)(score/4);
				if (deduction<0)
					((Bullet)fo).getShipCreator().score += Constants.KILL_POINTS;  // old code for fixed kill bonus
				else {
					((Bullet)fo).getShipCreator().score += deduction;
					score -= deduction;
				}
			}
			//if (lives == 0) die();
			//else semiDie();
			semiDie();
			return true;
		} 	
	}	
	
	protected void magnetNearbyBonuses(Universe u) {
		if (!this.isMagnetic()) return;
		//final float MAX_DIST = Util.distance(0, 0, Universe.WIDTH, Universe.HEIGHT );
		final int DIST_FACTOR = 15;
		//Universe u = this.getShipController().
		for (FlyingObject fo : u.getFlyingObjects()) {
			if(fo instanceof Bonus && !(fo instanceof BombBonus)){

				float foX = fo.getX();
				float foY = fo.getY();
				float dist = Util.distance(x, y, fo.getX(), fo.getY() );
				if (dist > this.getRadius()*DIST_FACTOR) continue;
				float angle = Util.calcAngle(foX, foY, x,y);
				dist = this.getRadius(); 
				float nextX = foX+(float)Math.cos(angle)*dist; //foX + (x - foX)/(180*(1-(MAX_DIST-dist)/MAX_DIST)); 
				float nextY = foY+(float)Math.sin(angle)*dist; //foY + (y - foY)/(180*(1-(MAX_DIST-dist)/MAX_DIST));
				//System.out.print("x,y="+nextX+","+nextY);
				//System.out.println("next x,y="+nextX+","+nextY);
				fo.setX(nextX);
				fo.setY(nextY);				
			}
		}
	}
	
	protected void die() {
		setAlive(false);
		sleepTime = Integer.MAX_VALUE;
		//sleepTime = SLEEP_TIME;
	}
	
	protected void semiDie() {
		setAlive(false);
		sleepTime = SLEEP_TIME;
		this.score -= Constants.RESPAWN_PENALTY;
	}
	
	@Override
	protected Color getColor() {
		return null;
		/*
		if(alive)
			return Color.GREEN;
		else
			return Color.BLUE;
			*/
	}

	public boolean isAlive() { return alive; }
	
	protected void setAlive(boolean alive) {   //was public until 11-18-2019 *****
		this.alive = alive;
		if(!alive){
			heading = 0;
			speed = 0;
		}
		else {
			resetLife();
		}
	}

	@Override
	public float getRadius() { return 12.0f; }   // ***** should refer to a constant or otherwise

	protected void decrementSleep() { if (isSleeping()) sleepTime--; }
	protected void decrementDisabled() { disabledCountdown = Integer.max(--disabledCountdown, 0); }
	protected void decrementMagnet() { magnetCountdown = Integer.max(--magnetCountdown, 0); }
	protected void decrementEMP() { empCountdown = Integer.max(--empCountdown, 0); }
	protected void decrementJump() { jumpCountdown = Integer.max(--jumpCountdown, 0); }

	public boolean canTeleport() { return jumpCountdown > 0 ;}
	protected void goTeleport() { jumpCountdown = 0; }
	
	protected Color amountColor ( int amount, int max, boolean trans){
		Color c = null;
		float amountRed = (float) ((1.0 - amount/(double)max));
		float amountGreen = (float) ((amount/(double)max));
		float amountBlue = 0;
		if (amountRed > 255) amountRed=255;
		if (amountGreen > 255) amountGreen =255;
		try {
		if (trans) c = new Color(amountRed,amountGreen,amountBlue,0.25f);
		else c = new Color(amountRed,amountGreen,amountBlue,1f);
		}
		catch (Exception e) {System.out.println("red="+amountRed+" green="+amountGreen+" blue="+amountBlue); }
			
		return c;
	}
	
	protected Color shieldColor( boolean trans) {
		if (trans)
			return amountColor(100, SHIELD_HEALTH_FOR_LIFE, trans); //new Color(0f,255f,0f,0.25f);
		else return Constants.shieldColor;
		//return amountColor(shield, SHIELD_HEALTH_FOR_LIFE, trans);
	}
	
	@Deprecated
	protected Color fuelColor( boolean trans) { return Constants.fuelColor; }
	@Deprecated
	protected Color bulletsColor( boolean trans) { return Constants.bulletsColor; }
	
	/** Paint the ship. */
	@Override
	public void paint(Graphics g) {
		
		decrementDisabled();
		decrementMagnet();
		decrementEMP();
		decrementJump();
		if (isSleeping()) {
			;
		}
		else {
	
			
		int col = 10;
		int rows = 4;
		
		int width = 36;
		int height = 36;
		
	
		//g.setColor(this.getColor());
		g.setColor(Color.GRAY);
		

		
		if(Constants.showShipNames) {
			if (!Constants.SCALED_UNIVERSE)
				g.drawString(shipController.getName(),
					(int)this.getX() + Universe.viewPortXOffSet,
					Constants.height - (int)this.getY()+25 + Universe.viewPortYOffSet);
			else {
				
				// computing again ... BAD!!!
				int sX = (int)(x*Constants.scaleX);
				int sY = Constants.height - (int)(y*Constants.scaleY) + 20;
				

				//g.drawString(shipController.getName(),
				//		sX,
				//		sY);
					
				Visualizer.drawString(g,shipController.getName(),sX-(shipController.getName().length()*7)/2,sY+7,2);
				
				// display shield strength
				int length = (int) ((this.getShieldHealth() / (double) highestShield) * 50);
				g.setColor( shieldColor(false));
				g.fillRect(sX-25,sY,length,3);				
				g.setColor(Color.WHITE);
				g.drawRect(sX-25,sY,50,3);

				// display fuel
				length = (int) ((fuel / (double) highestFuel) * 50);
				sY -= 5; //Constants.height - (int)(y*Constants.scaleY) + 10;
				g.setColor( Constants.fuelColor);
				g.fillRect(sX-25,sY,length,3);
				g.setColor(Color.WHITE);
				g.drawRect(sX-25,sY,50,3);

				// display fuel
				length = (int) ((bullets / (double) highestBullets) * 50);
				sY -= 5; //Constants.height - (int)(y*Constants.scaleY) + 10;
				g.setColor( Constants.bulletsColor);
				g.fillRect(sX-25,sY,length,3);
				g.setColor(Color.WHITE);
				g.drawRect(sX-25,sY,50,3);

				// display lives
				length = (int) ((lives / (double) highestLives) * 50);
				sY -= 5; //Constants.height - (int)(y*Constants.scaleY) + 10;
				g.setColor( Constants.livesColor);
				g.fillRect(sX-25,sY,length,3);
				g.setColor(Color.WHITE);
				g.drawRect(sX-25,sY,50,3);
			}
				
		}
		
		int xMod=0;
		int yMod=0;
		
		float angle = facing;
		
		
		angle-= Math.PI/2.0f;
		angle-= Math.PI/16.0f;
		//System.out.println(angle);
		
		while(angle >= Math.PI*2){
			angle-=Math.PI*2;
		}
		
		if(angle < 0)
			angle+=Math.PI*2.0f;

		//System.out.println(25/((float)(col * rows)));
		
		int i;
		for(i = 1; i <= (col * rows); i++){

			if(angle < 2.0f*Math.PI*(i/((float)(col * rows)))){
			//	System.out.println(i);
				

				break;
			}	
		}
		
		i = 40 - i;
		
		for(int j = 0; j < i-1; j++){
			xMod++;
			if(xMod == col){
				xMod = 0;
				yMod++;
			}
		}
		
		yMod+=(type%8)*4;
		
		//System.out.println(xMod + "\t" + yMod);
		
		xMod*=width;
		yMod*=height;
		
		/*
    img - the specified image to be drawn
    dx1 - the x coordinate of the first corner of the destination rectangle.
    dy1 - the y coordinate of the first corner of the destination rectangle.
    dx2 - the x coordinate of the second corner of the destination rectangle.
    dy2 - the y coordinate of the second corner of the destination rectangle.
    sx1 - the x coordinate of the first corner of the source rectangle.
    sy1 - the y coordinate of the first corner of the source rectangle.
    sx2 - the x coordinate of the second corner of the source rectangle.
    sy2 - the y coordinate of the second corner of the source rectangle.
    observer - object to be notified as more of the image is scaled and converted.
    */
    
		if (!Constants.SCALED_UNIVERSE)
		g.drawImage(
				shipImage,
				0+(int)x-width/2 + Universe.viewPortXOffSet,
				Constants.height - (0+(int)y-height/2) + Universe.viewPortYOffSet,
				width+(int)x-width/2 + Universe.viewPortXOffSet,
				Constants.height - (height+(int)y-height/2) + Universe.viewPortYOffSet,
				0+xMod,
				height+yMod,
				width+xMod,
				0+yMod,
				null);
		else {
			int sWidth = (int)(width*Constants.scaleX);
			int sHeight = (int)(height*Constants.scaleY);
			int sX = (int)(x*Constants.scaleX);
			int sY = (int)(y*Constants.scaleY);
			g.drawImage(
					shipImage,
					sX-sWidth/2,
					Constants.height - (0+(int)sY-sHeight/2),
					sWidth+sX-sWidth/2,
					Constants.height - (sHeight+sY-sHeight/2),
					
					0+xMod,
					height+yMod,
					width+xMod,
					0+yMod,
					null);
		}
		
		// draw shield
		if (isShieldUp()) {

			if(hitRecently){
				g.setColor(Color.WHITE);
				hitRecently = false;
			}
			else{
				g.setColor( shieldColor(true));
			}
			if (!Constants.SCALED_UNIVERSE) {
			g.drawOval(
					(int)(x-this.getRadius()-10) + Universe.viewPortXOffSet,
					Constants.height - (int)(y+this.getRadius()+10) + Universe.viewPortYOffSet,
					(int)(this.getRadius()*2.0f+20) ,
					(int)(this.getRadius()*2.0f+20) );
			g.drawOval(
					(int)(x-this.getRadius()-9) + Universe.viewPortXOffSet,
					Constants.height - (int)(y+this.getRadius()+9) + Universe.viewPortYOffSet,
					(int)(this.getRadius()*2.0f+18),
					(int)(this.getRadius()*2.0f+18));
			}
			else {
//				 computing again ... BAD!!!
				int sWidth = (int)(width*Constants.scaleX);
				int sHeight = (int)(height*Constants.scaleY);
				int sX = (int)(x*Constants.scaleX);
				int sY = (int)(y*Constants.scaleY);
				int wid = (int)(40*Constants.scaleX);
				int high = (int)(40*Constants.scaleY);
				g.fillOval(
						(int)(sX-(sWidth/2) + 1) ,
						Constants.height - (int)(sY+(sHeight/2)+ 1),
						wid - 1,
						high - 1 );
			}
		}
				
		score++;
		
		if (fireBulletWait > 0) fireBulletWait--;
		
		//if (shieldUp()) {shieldLeft--; shieldCurrent--;}
		if (isShieldUp()) { 
			// score--;  // previously no points were earned while the shield was up
			if (shield > 0) shield -= 1;
			else toggleShield();
			}
		//else if (shieldWait>0) {shieldWait--;}
		}
	}

	/** @return The controller tied to this ship. */
	public ShipController getShipController() { return shipController; }
	
	/** Link this ship to a ship controller. */
	protected void setShipController(ShipController shipController) { this.shipController = shipController; }

	/**
	 * Return true if the ship should be removed for leaving the universe. 
	 * Currently returns false as ships wrap around (and are not removed).
	 * @param gd
	 * @return
	 */
	protected boolean handleOffUniverse(Universe gd) {
		return false;
		
		/*
		if (this.offUniverse()) {
			lives--;
			if(Constants.log) Constants.logWriter.logShipDeath((Ship)this,(FlyingObject)null);
			//gd.getExplosion().add(new Explosion(this.getX(),this.getY()));
			gd.addExplosion(this.getX(),this.getY());
			if (Constants.SOUND) Sound.soundShipExplode.play();
			
			//if (lives == 0) die();
			//else semiDie();
			
			semiDie();
			
			//System.out.println("lives now = " + lives);
			return true;
		}
		else return false;
		*/
	}
	
	/** @return The amount of shield health remaining. 0 indicates expired. */
	public int getShieldHealth(){ return this.shield; }
	
	/** @return The amount of bullets remaining. */
	public int getBulletsAmmount(){ return this.bullets; }
	
	/** @return The amount of laser shots remaining. */
	public int getLaserAmount(){ return this.laserEnergy; }
	
	/** @return The amount of fuel remaining. */
	public int getFuelAmmount(){ return this.fuel; }
	
	/** 
	 * @deprecated No longer used. 
	 * @return The amount of lives remaining.
	 * */
	@Deprecated
	public int getLives(){ return this.lives; }
	//public int getType(){ return this.type; } // The sprite image to use (one per ship). */

	/** @return The current weaponLevel. */
	public int getWeaponLevel() { return weaponLevel; }

	/**
	 * Change acceleration, reduce fuel based on amount of acceleration
	 */
	protected void setAcceleration(float acceleration) {  //was public until 11-18-2019 *****
		if (!(this instanceof AIShip)) {
			if (fuel > 0) {
				this.acceleration = acceleration;
				fuel -= Math.abs((acceleration * 10f));
				if (fuel < 0)
					fuel = 0;
			}
			else this.acceleration = 0;
		}
		else this.acceleration = acceleration;
	}
}
