package edu.unk.fun330;

import java.awt.*;

public class LifeBonus extends Bonus {
	
	public LifeBonus(float x, float y, int amount) { super(x, y, amount); }
	public Color getColor() { return Color.YELLOW; }
}
