package edu.unk.fun330;

import java.awt.Color;
import java.awt.Graphics;

public class AIShip extends Ship {

	private int invisibleCount = 0;
	private static final int MAX_INVISIBLE_COUNT = 100;
	private int invisibleWait;
	private static final int INVISIBLE_WAIT = 5;
	private int teleportCount = 0;
	private static final int MAX_TELEPORT_COUNT = 5;

	protected int bulletFireWait() { return 5; }
	
	public Color getColor() {
		return null;
	}
	
	public float getRadius() {
		return 12.0f;
	}

	public AIShip(float x, float y, int type) {
		super(x, y, type);
	}
	
	protected void resetLife() {
		shield = 0;
		shieldUp = false;
		sleepTime = 0;
		weaponLevel = 1;
		bullets = INIT_BULLETS;
		fuel = INIT_FUEL;
		lives = INIT_LIVES;
	}
	
	public boolean isInvisible() { return invisibleCount > 0; }
	public boolean canGoInvisible() { return invisibleWait==0 && invisibleCount == 0; }
	protected void makeInvisible() { if (canGoInvisible()) this.invisibleCount = MAX_INVISIBLE_COUNT; }
	public void makeVisible(){invisibleCount = 0;}
	
	public boolean canTeleport() { return teleportCount == 0 ;}
	/*
	protected void goTeleport() {
		if (canTeleport() )
			this.teleportCount = MAX_TELEPORT_COUNT;
		}
		*/

	
    public void paint(Graphics g) {
    	
    	if (invisibleCount > 0) {
    		invisibleCount--;
    		if (invisibleCount==0) invisibleWait = INVISIBLE_WAIT;
    		g.setColor(Color.WHITE);
    		/*
    		g.drawOval((int)(this.getX()-this.getRadius()-10),
    				Constants.height - (int)(this.getY()+this.getRadius()+10),
    				(int)(this.getRadius()*2.0f+20),
    				(int)(this.getRadius()*2.0f+20));
    				*/
    	}
    	else {
    		super.paint(g);
    		if (invisibleWait>0) {invisibleWait--;}
    	}
    	
    	if (teleportCount > 0) teleportCount--;
		
		
		
		
	}
}
