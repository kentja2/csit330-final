package edu.unk.fun330.base.effects;

import java.awt.Image;

public class Exhaust extends Effect {
	
	public static Image exhaustImage;
	protected Image getImage() { return Exhaust.exhaustImage; }
	
	public Exhaust(float x, float y){
		super(19, 1, 19, 16, 16);
		this.x = x;
		this.y = y;
	}
}
