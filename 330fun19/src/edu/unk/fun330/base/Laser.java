package edu.unk.fun330.base;

import edu.unk.fun330.Ship;

public class Laser extends Bullet {

	public Laser(float x, float y, Ship shipCreator) {
		super(x,y,shipCreator,2);
		life = MAX_LIFE/5;
	}
}
